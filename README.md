# Minecraft Discord Bot

## Beschreibung
Erlaubt infos von einem Minecraft Server abzufragen und erlaubt es einen User auf die Whitelist zu setzen.

## Command overview
| Command | Beschreibung |
| --- | --- |
| !ping | Pingt den DC Bot and  |
| !status | Zeigt Infos über den MCServer an |
| !whitelist add {name} | Fügt einen User auf die Whitelist hinzu, wenn eine Bestätigun vom Admin kommt in form von 👍 oder 👎 |
