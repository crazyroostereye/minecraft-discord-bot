FROM python:3.9

WORKDIR /app

COPY ./src/requirements.txt ./

RUN python -m pip install -r requirements.txt

COPY ./src/* .

ENV DISCORD=
ENV EXTERNAL=
ENV INTERNAL=
ENV RCON_PORT="25575"
ENV QUERY_PORT="25565"
ENV MC_PORT="25565"
ENV ADMIN_ROLE_ID=
ENV RCON_PASSWORD=

CMD ["python", "-u", "main.py"]