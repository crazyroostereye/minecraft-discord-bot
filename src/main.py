import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
from mctools import QUERYClient, RCONClient, PINGClient, errors
import json
import re 
import traceback
"""
Created by: Julian Beittel
Date: 2023-04-07
Copyrigth: CC-SA

Description:
This is a discord bot for a minecraft server

Die .env Datei muss folgende Variablen enthalten:
KEY=Dicord Bot Token
mcserver=minecraft server ip (für pings aka verwende externen zugriff)
rconserver= rcon / quarry server ip (für whitelist und sollte wen hosted mit server mit localhost sein oder locale ip)
rconpass=rcon passwort
adminid=Die discord id des admins (Der mit dem privileg jemanden auf die whitelist zu setzen)
"""

def remove_color_codes(input_string):
    # Define a regular expression pattern to match ANSI escape sequences (color codes)
    ansi_escape_pattern = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')

    # Use the sub() method to replace matched color codes with an empty string
    clean_string = ansi_escape_pattern.sub('', input_string)

    return clean_string

# Load the .env file
#load_dotenv()

#Load Variables
TOKEN = os.getenv('DISCORD')
Admin = int(os.getenv('ADMIN_ROLE_ID'))
server = os.getenv("EXTERNAL")
port = os.getenv("MC_PORT")
qport = os.getenv("QUERY_PORT")
rcon = os.getenv("INTERNAL")
rport = os.getenv("RCON_PORT")
rpass = os.getenv("RCON_PASSWORD")
approle = int(os.getenv("ADMIN_ROLE_ID"))

# Create the bot
intends = discord.Intents.default()
intends.message_content = True
bot = commands.Bot(command_prefix='!', intents=intends)

@bot.command(name='ping', description='Pings den bot')
async def ping(ctx):
    print("PONG!")
    await ctx.send('pong')

@bot.command(name='status', description='Zeigt den Status des Servers an')
async def status(ctx):
    # Define the server variables
    print(rcon)
    print("Hello")
    try:
        with QUERYClient(rcon, qport, timeout=5) as query:
            stats = query.get_full_stats()
        with PINGClient(server, port, timeout=5) as ping:
            elapsed = ping.ping()
        
        #Create the embed
        emebedvar = discord.Embed(title="Server Status", color=0x00ff00)
        emebedvar.add_field(name="Version", value=stats["version"], inline=False)
        emebedvar.add_field(name="Message of the Day", value=remove_color_codes(stats["motd"]), inline=False)
        emebedvar.add_field(name="Spieler Zahl", value=f'{stats["numplayers"]}/{stats["maxplayers"]}', inline=False)
        if stats["numplayers"] == "0":
            namelist = 'Keine Spieler online'
        else:
            namelist_a = []
            for i in stats["players"]:
                namelist_a.append(remove_color_codes(i))
            namelist = '\n'.join(namelist_a)
        emebedvar.add_field(name="Spieler", value=namelist, inline=False)
        emebedvar.add_field(name="Ping", value=f'{round(elapsed, 2)}ms', inline=False)
    except:
        #traceback.print_exc()
        emebedvar = discord.Embed(title="Server Status", color=0xff1100)
        emebedvar.add_field(name="Status", value="offline", inline=False)
    finally:
    # send the embed
        await ctx.send(embed=emebedvar)
    
@bot.command(name='whitelist', 
             description='Whitelist commmand für den Server\nDer Admin muss die Anfrage bestätigen mit einem 👍 oder 👎\n!whitelist add Spielername',
             args={"arg1": "add oder remove", "arg2": "Spielername"})
async def whitelist(ctx, arg1, arg2):
    if arg1 == "add":
        await ctx.message.add_reaction("👍")
        await ctx.message.add_reaction("👎")
        

@bot.event
async def on_reaction_add(reaction, user):
    # The Message of the Reaction
    message = reaction.message
    if "!whitelist add " in message.content and user != bot.user and user.get_role(Admin) != None:
        # print(user.get_role(Admin), type(user.get_role(Admin)))
        if reaction.emoji == "👍":
            with RCONClient(rcon, rport) as rcm:
                rcm.login(rpass)
                resp = rcm.command(f'whitelist add {message.content[len("!whitelist add "):]}')
                print(resp)
            if resp == "Player is already whitelisted":
                await message.channel.send(resp)
                await message.author.add_roles(user.guild.get_role(approle))
            elif resp == "That player does not exist":
                await message.channel.send(f"{message.author.mention} Dieser Name existiert nicht")
            else:
                await message.channel.send(f"{re.compile(re.escape(message.content[len('!whitelist add '):]), re.IGNORECASE).sub(message.author.mention, remove_color_codes(resp))}")
                await message.author.add_roles(user.guild.get_role(approle))
        elif reaction.emoji == "👎":
            await message.channel.send(f'{message.author.mention} wurde abgelehnt')

bot.run(TOKEN)